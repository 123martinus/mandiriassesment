﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Model;
using MediatR;
using System.Reflection.Metadata;
using WebApplication1.Command;
using WebApplication1.Feature.DeleteChurch;
using WebApplication1.Feature.GetListChurch;
using Microsoft.AspNetCore.Http.HttpResults;
using WebApplication1.Feature.UpdateChurch;
namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChurchController : ControllerBase
    {

        private readonly ILogger<ChurchController> _logger;
        private readonly IMediator mediator;

        public ChurchController(ILogger<ChurchController> logger, IMediator mediator)
        {
            _logger = logger;
            this.mediator = mediator;
        }
        [HttpPost]
        public async Task<IActionResult> Create(CreateChurchCommand command)
        {
            return Ok(await mediator.Send(command));
        }
        [HttpGet("{id}", Name = "ChurchById")]
        public async Task<ActionResult> GetById(int id)
        {

            var responseChurch = await mediator.Send(new GetDetailChurchCommand{ Id = id});
            return responseChurch != null ? Ok(responseChurch) : NotFound();
        }
        [HttpGet]
        public async Task<ActionResult> GetAllChurch()
        {

            var responseChurch = await mediator.Send(new GetListChurchCommand());
            return Ok(responseChurch);
        }


        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateChurchById(int id, UpdateChurchCommand updateChurchCommand)
        {
            updateChurchCommand.church.churchId = id;
            var responseChurch = await mediator.Send(updateChurchCommand);
            return Ok(responseChurch);
        }

        [HttpDelete("{id}", Name = "Delete Church By Id")]
        public async Task<ActionResult> DeleteChurchById(int id)
        {
            var responseChurch = await mediator.Send(new DeleteChurchCommand { Id = id});
            return Ok(responseChurch);
        }
    } 
}
