﻿using MediatR;
using WebApplication1.Model.Response;

namespace WebApplication1.Feature.DeleteChurch
{
    public class GetDetailChurchCommand : IRequest<CreateChurchResponse>
    {
        public int Id { get; set; }
    }
}
