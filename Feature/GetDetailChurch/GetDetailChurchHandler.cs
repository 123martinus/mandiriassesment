﻿using AutoMapper;
using MediatR;
using WebApplication1.Command;
using WebApplication1.Context;
using WebApplication1.Feature.DeleteChurch;
using WebApplication1.Model;
using WebApplication1.Model.Response;

namespace WebApplication1.Feature.GetDetailChurch
{
    public class GetDetailChurchHandler : IRequestHandler<GetDetailChurchCommand, CreateChurchResponse>
    {
        private ChurchContextDB context;
        private IMapper _mapper;
        public GetDetailChurchHandler(ChurchContextDB context, IMapper mapper)
        {
            this.context = context;
            _mapper = mapper;
        }
        public async Task<CreateChurchResponse> Handle(GetDetailChurchCommand command, CancellationToken cancellationToken)
        {
            Church? detailChurch = context.Church.FirstOrDefault(church => church.churchId == command.Id);
            var response = new CreateChurchResponse();
            if (detailChurch == null)
            {
                response.message = "data tidak ada";
                response.code = "500";
                return _mapper.Map<CreateChurchResponse>(response);
            }
            response = new CreateChurchResponse(detailChurch, "200", "data berhasil didapat");


            return _mapper.Map<CreateChurchResponse>(response);
        }

    }
}
