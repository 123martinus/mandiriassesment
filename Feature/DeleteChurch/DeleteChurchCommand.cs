﻿using MediatR;
using WebApplication1.Model.Response;

namespace WebApplication1.Feature.DeleteChurch
{
    public class DeleteChurchCommand : IRequest<CreateChurchResponse>
    {
        public int Id { get; set; }
    }
}
