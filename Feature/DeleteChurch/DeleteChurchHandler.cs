﻿using AutoMapper;
using MediatR;
using WebApplication1.Context;
using WebApplication1.Model;
using WebApplication1.Model.Response;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace WebApplication1.Feature.DeleteChurch
{
    public class DeleteChurchHandler : IRequestHandler<DeleteChurchCommand, CreateChurchResponse>
    {
        public ChurchContextDB contextDB;
        public IMapper mapper;
        public DeleteChurchHandler(ChurchContextDB contextDB, IMapper mapper)
        {
            this.contextDB = contextDB;
            this.mapper = mapper;
        }
        public async Task<CreateChurchResponse> Handle(DeleteChurchCommand request, CancellationToken cancellationToken)
        {
            var church = contextDB.Church.Where(c => c.churchId == request.Id).FirstOrDefault();
            CreateChurchResponse response = new CreateChurchResponse();
            if (church == null)
            {
                response.code = "400";
                response.message = "Data tidak ditemukan";

                return mapper.Map<CreateChurchResponse>(response);
            }
            contextDB.Church.Remove(church);
            await contextDB.SaveChangesAsync();

            response = new CreateChurchResponse(null, "200", "Data Berhasil dihapus");
            return mapper.Map<CreateChurchResponse>(response);
        }
    }
}
