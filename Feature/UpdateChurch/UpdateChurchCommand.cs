﻿using MediatR;
using WebApplication1.Model;
using WebApplication1.Model.Response;

namespace WebApplication1.Feature.UpdateChurch
{
    public class UpdateChurchCommand : IRequest<CreateChurchResponse>
    {
        public Church church { get; set; }
    }
}
