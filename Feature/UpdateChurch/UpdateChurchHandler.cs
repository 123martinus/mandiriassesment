﻿using AutoMapper;
using MediatR;
using WebApplication1.Context;
using WebApplication1.Model;
using WebApplication1.Model.Response;

namespace WebApplication1.Feature.UpdateChurch
{
    public class UpdateChurchHandler : IRequestHandler<UpdateChurchCommand, CreateChurchResponse>
    {
        private ChurchContextDB contextDB;
        private IMapper mapper;

        public UpdateChurchHandler(ChurchContextDB contextDB, IMapper mapper)
        {
            this.contextDB = contextDB;
            this.mapper = mapper;
        }

        public async Task<CreateChurchResponse> Handle(UpdateChurchCommand request, CancellationToken cancellationToken)
        {
            Church church = contextDB.Church.Where(c => c.churchId == request.church.churchId).FirstOrDefault();

            CreateChurchResponse response = new CreateChurchResponse();
            if (church == null)
            {
                response.code = "200";
                response.message = "Data tidak ditemukan";
                return mapper.Map<CreateChurchResponse>(response);
            }
            
            church.churchName = request.church.churchName;
            church.leaderOfChurch = request.church.leaderOfChurch;
            church.totalOfCongregation = request.church.totalOfCongregation;
            church.address = request.church.address;
            await contextDB.SaveChangesAsync();

            response.church = church;
            response.message = "Data sukses diupdate";
            response.code = "200";

            return mapper.Map<CreateChurchResponse>(response);
        }
    }
}
