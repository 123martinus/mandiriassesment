﻿using MediatR;
using WebApplication1.Context;
using WebApplication1.Model;
using WebApplication1.Model.Response;

namespace WebApplication1.Command
{
    public class CreateChurchCommand : IRequest<CreateChurchResponse>
    {
        public Church commandChurch {  get; set; } 
    }
}
