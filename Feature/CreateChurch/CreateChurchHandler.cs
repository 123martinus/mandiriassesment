﻿using MediatR;
using System.Text.Json;
using AutoMapper;
using WebApplication1.Command;
using WebApplication1.Context;
using WebApplication1.Model;
using WebApplication1.Model.Response;

namespace WebApplication1.Feature.CreateChurch
{
    public class CreateChurchCommandHandler : IRequestHandler<CreateChurchCommand, CreateChurchResponse>
    {
        private ChurchContextDB context;
        private IMapper _mapper;
        public CreateChurchCommandHandler(ChurchContextDB context, IMapper mapper)
        {
            this.context = context;
            _mapper = mapper;
        }
        public async Task<CreateChurchResponse> Handle(CreateChurchCommand command, CancellationToken cancellationToken)
        {
            var initialChurch = new Church();
            initialChurch = command.commandChurch;
            context.Church.Add(initialChurch);
            await context.SaveChangesAsync();
            Church? savedChurch = context.Church.FirstOrDefault(church => church.churchId == command.commandChurch.churchId );
            var response = new CreateChurchResponse();
            if (savedChurch == null)
            {
                response.message = "data gagal disimpan";
                response.code = "500";
                return _mapper.Map<CreateChurchResponse>(response);
            }
            response = new CreateChurchResponse(savedChurch, "200", "data berhasil disimpan");


            return _mapper.Map<CreateChurchResponse>(response);
        }
    }
}
