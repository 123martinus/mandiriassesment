﻿using AutoMapper;
using Azure;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Context;
using WebApplication1.Model;
using WebApplication1.Model.Response;

namespace WebApplication1.Feature.GetListChurch
{
    public class GetListChurchHandler : IRequestHandler<GetListChurchCommand, GetListChurchResponse>
    {

        private ChurchContextDB contextDB;
        private IMapper _mapper;

        public GetListChurchHandler (ChurchContextDB contextDB ,  IMapper mapper)
        {
            this.contextDB = contextDB;
            _mapper = mapper;
        }

        public async Task<GetListChurchResponse> Handle(GetListChurchCommand request, CancellationToken cancellationToken)
        {
            List<Church> listOfChurch = await contextDB.Church.ToListAsync();

            GetListChurchResponse response = new GetListChurchResponse(listOfChurch, "200", "data berhasil disimpan");


            return _mapper.Map<GetListChurchResponse>(response);
        }
    }
}
