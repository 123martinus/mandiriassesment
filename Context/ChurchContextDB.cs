﻿using Microsoft.EntityFrameworkCore;
using WebApplication1.Model;

namespace WebApplication1.Context
{
    public class ChurchContextDB : DbContext
    {
        public ChurchContextDB() { }
        public ChurchContextDB(DbContextOptions<ChurchContextDB> options) : base(options) { 
        }

        public DbSet<Church> Church{ get; set; }
    }
}
