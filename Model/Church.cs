﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Swashbuckle.AspNetCore.Annotations;

namespace WebApplication1.Model
{
    public class Church
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [SwaggerSchema(ReadOnly = true)]
        public int churchId { get; set; }
        public string churchName { get; set; }
        public string address { get; set; }
        public string leaderOfChurch { get; set; }
        public string totalOfCongregation { get; set; }
    }
}
