﻿namespace WebApplication1.Model.Response
{
    public class CreateChurchResponse
    {
        public Church church { get; set; }
        public string message { get; set; }
        public string code { get; set; }

        public CreateChurchResponse () { }
        public CreateChurchResponse(Church church, string code, string message) { 
            this.church = church;
            this.message = message;
            this.code = code;
        }
    }
}
