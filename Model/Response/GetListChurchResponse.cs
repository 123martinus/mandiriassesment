﻿namespace WebApplication1.Model.Response
{
    public class GetListChurchResponse
    {
        public List<Church> churchList { get; set;}
        public string message { get; set; }
        public string code { get; set; }

        public GetListChurchResponse() { }
        public GetListChurchResponse(List<Church> churchList, string code, string message)
        {
            this.churchList = churchList;
            this.message = message;
            this.code = code;
        }
    }
}
